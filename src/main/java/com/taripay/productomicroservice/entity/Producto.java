package com.taripay.productomicroservice.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

/**
 *
 * @author roberth
 */
@Table(value="tb_producto")
@Getter
@Setter
@Builder
public class Producto {
    
    @Id
    private Long id;
    private String codigo;
    private String descripcion;
    private double precio;
    private int stock;
    private Long tiendaId;
}
