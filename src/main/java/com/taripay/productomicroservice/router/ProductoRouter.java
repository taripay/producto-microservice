package com.taripay.productomicroservice.router;

import com.taripay.productomicroservice.handler.ProductoHandler;
import org.springframework.boot.autoconfigure.web.WebProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

/**
 *
 * @author roberth
 */
@Configuration
public class ProductoRouter {
    
    private String path = "api/producto";
    
    @Bean
    public WebProperties.Resources resources(){
        return new WebProperties.Resources();
    }
    
    @Bean
    public RouterFunction<ServerResponse> routerProducto(ProductoHandler handler){
        return RouterFunctions.route()
                .GET(path + "/tienda/{tiendaId}", handler::getByTienda)
                .POST(path, handler::save)
                .build();
    }
}
