package com.taripay.productomicroservice.handler;

import com.taripay.productomicroservice.entity.Producto;
import com.taripay.productomicroservice.service.ProductoService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

/**
 *
 * @author roberth
 */
@Component
@Getter
@Setter
@RefreshScope
public class ProductoHandler {
    
    private final MediaType typeJson = MediaType.APPLICATION_JSON;
    
    @Autowired
    ProductoService productoService;
    
    public Mono<ServerResponse> getByTienda(ServerRequest request){
        Long tiendaId = Long.parseLong(request.pathVariable("tiendaId"));
        return ServerResponse.ok()
                .contentType(typeJson)
                .body(productoService.getByTienda(tiendaId), Producto.class);
    }
    
    public Mono<ServerResponse> save(ServerRequest request){
        Mono<Producto> modelStream = request.bodyToMono(Producto.class);
        
        return modelStream.flatMap(model -> ServerResponse.ok()
                .contentType(typeJson)
                .body(productoService.save(model), Producto.class)
        );
    }
}
