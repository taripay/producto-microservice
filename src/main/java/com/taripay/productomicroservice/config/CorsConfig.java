package com.taripay.productomicroservice.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.config.CorsRegistry;
import org.springframework.web.reactive.config.WebFluxConfigurer;

/**
 *
 * @author roberth
 */
@Configuration
public class CorsConfig implements WebFluxConfigurer {
    
    @Override
    public void addCorsMappings(CorsRegistry registry){
        registry.addMapping("/**")
                .allowedMethods("GET", "POST", "PUT", "PATCH", "DELETE")
                .allowedOriginPatterns("*")
                .allowCredentials(false);
    }
}
