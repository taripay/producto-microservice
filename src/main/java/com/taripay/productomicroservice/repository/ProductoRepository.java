package com.taripay.productomicroservice.repository;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import com.taripay.productomicroservice.entity.Producto;
import reactor.core.publisher.Flux;

/**
 *
 * @author roberth
 */
@Repository
public interface ProductoRepository extends ReactiveCrudRepository<Producto, Long>{
    
    public Flux<Producto> findByTiendaId(Long tiendaId);
    
}
