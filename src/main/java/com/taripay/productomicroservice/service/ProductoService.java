package com.taripay.productomicroservice.service;

import com.taripay.productomicroservice.entity.Producto;
import com.taripay.productomicroservice.repository.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 *
 * @author roberth
 */
@Service
public class ProductoService {
    
    @Autowired
    ProductoRepository productoRepository;
    
    public Flux<Producto> getByTienda(Long tiendaId){
        return productoRepository.findByTiendaId(tiendaId);
    }
    
    public Mono<Producto> save (Producto model){
        return productoRepository.save(model);
    }
}
